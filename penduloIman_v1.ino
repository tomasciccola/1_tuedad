//#define DEBUG

#define IN_POTE 0

#define OUT_IMAN_1 10
#define OUT_IMAN_2 9
#define ENABLE 8

int MEDIO_RANGE[] = {520, 540};

const int noiseThreshold = 5;
int prevVal;

void setup() {
  
  Serial.begin(9600);
  prevVal = 0;

  //config imán
  pinMode(OUT_IMAN_1, OUTPUT);
  pinMode(OUT_IMAN_2, OUTPUT);
  pinMode(ENABLE, OUTPUT);

  //empezá apagado
  digitalWrite(ENABLE, LOW);

  initSeq();


}

void loop() {
  int val = analogRead(IN_POTE);

  if (newValue(prevVal, val, noiseThreshold)) {
    Serial.println(val);
    if (val < MEDIO_RANGE[0] || val > MEDIO_RANGE[1]) {

      //sube a la derecha
      if (val - prevVal < 0 && val < MEDIO_RANGE[0]) {

#ifdef DEBUG
        Serial.println("D");
#endif

        repeler();
        
      //baja desde la derecha
      } else if (val - prevVal > 0 && val < MEDIO_RANGE[0]) {

#ifdef DEBUG
        Serial.println("D");
#endif

        atraer();

      //sube a la izquierda
      } else if (val - prevVal > 0 && val > MEDIO_RANGE[1]) {

#ifdef DEBUG
        Serial.println("I");
#endif

        repeler();

      //baja desde la izquierda
      } else if (val - prevVal < 0 && val > MEDIO_RANGE[1]) {

#ifdef DEBUG
        Serial.println("I");
#endif

        atraer();
      }
    } else {

#ifdef DEBUG
      Serial.println("000");
#endif

      nada();

    }



    prevVal = val;
  }

}

bool newValue(int prevVal, int val, int threshold) {
  return val >= (prevVal + threshold) || val <= (prevVal - threshold);
}

void repeler() {
  digitalWrite(ENABLE, HIGH);
  digitalWrite(OUT_IMAN_1, HIGH);
  digitalWrite(OUT_IMAN_2, LOW);
}

void atraer() {
  digitalWrite(ENABLE, HIGH);
  digitalWrite(OUT_IMAN_1, LOW);
  digitalWrite(OUT_IMAN_2, HIGH);
}

void nada() {
  digitalWrite(ENABLE, LOW);
  digitalWrite(OUT_IMAN_1, LOW);
  digitalWrite(OUT_IMAN_2, LOW);
}

void initSeq() {


  int nTimes = 3;
  
  for (int i = 0; i < nTimes; i++) {
    atraer();
    delay(100);
    repeler();
    delay(100);
  }
 

}
